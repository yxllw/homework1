import animals.Kotik;

import java.util.Objects;

public class Application {
    public static void main(String[] args) {
        // вызов конструктора с параметрами
        Kotik tom = new Kotik("Tom", "loud", 1, 8);
        // вызов конструктора без параметров, использование сеттеров
        Kotik jerry = new Kotik("Jerry");
        jerry.setVoice("mau mau");
        jerry.setSatiety(31);
        jerry.setWeight(11);

        // вызов метода liveAnotherDay()
        for (String i: tom.liveAnotherDay()
             ) {
            System.out.println(i);
        }

        // Вывести в консоль имя и вес любого созданного экземпляра котика.
        System.out.println("\nИмя: " + jerry.getName() + ", Вес: " + jerry.getWeight() + "\n");

        // Вывести на экран результат сравнения одинаково ли разговаривают котики.
        boolean result = compareVoice(tom, jerry);
        System.out.println(result + "\n");

        // количество котиков
        System.out.println("Количество созданных котиков = " + Kotik.getCount());
    }

    public static boolean compareVoice(Kotik tom, Kotik jerry) {
        if(tom == null || jerry == null) {
            return false;
        }
        return Objects.equals(tom.getVoice(), jerry.getVoice());
    }
}
