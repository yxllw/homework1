package animals;

import model.Size;

public class Cow extends Herbivore implements Voice {
    public Cow(String name) {
        super(name);
    }

    public String getVoice() {
        return "me eeee";
    }
    public int getSatiety() {
        return satiety;
    }
    public Size getSize() {
        return Size.LARGE;
    }
}
