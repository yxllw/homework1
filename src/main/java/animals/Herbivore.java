package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {
    public Herbivore(String name) {
        super(name);
    }
    public void eat(Food food) throws WrongFoodException {
        if(food instanceof Meat) {
            throw new WrongFoodException("Такая еда не подходит для этого животного.");
        }
        else {
            satiety += 20;
            System.out.println("Животное покормлено.");
        }
    }
}
