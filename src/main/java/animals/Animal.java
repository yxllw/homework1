package animals;

import food.Food;
import food.WrongFoodException;
import model.Size;


public abstract class Animal {
    int satiety;
    String name;
    Animal(String name) {
        this.name = name;
    }
    public abstract void eat(Food food) throws WrongFoodException;
    public int getSatiety() {
        return 0;
    }
    public abstract Size getSize();
    public String getName() {
        return name;
    }
}
