package animals;

import model.Size;

public class Kotik extends Carnivorous implements Run, Voice {
    private String name;
    private String voice;
    private int weight;
    private static int count = 0;
    private static final int METHODS = 5;

    public Kotik(String name) {
        super(name);
        count++;
    }
    public Kotik(String name, String voice, int satiety, int weight) {
        super(name);
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        count++;
    }

    public boolean play() {
        if(satiety > 0) {
            satiety--;
            return true;
        } else {
        return false; }
    }
    public boolean sleep() {
        if(satiety > 0) {
            satiety--;
            return true;
        } else {
        return false; }
    }
    public boolean wash() {
        if(satiety > 0) {
            satiety--;
            return true;
        } else {
        return false; }
    }
    public boolean walk() {
        if(satiety > 0) {
            satiety--;
            return true;
        } else {
        return false; }
    }
    public boolean hunt() {
        if(satiety > 0) {
            satiety--;
            return true;
        } else {
        return false; }
    }
    public void run() {
        System.out.println("Kotik is running");
    }
    public void eat(int satietyNumber) {
        this.satiety += satietyNumber;
    }
    public void eat(int satietyNumber, String foodName) {
        this.satiety += satietyNumber;
    }
    public void eat() {
        int satietyNumber = 30;
        String foodName = "chicken";
        eat(satietyNumber, foodName);
    }

    public String[] liveAnotherDay() {
        String[] schedule = new String[24];

        for(int i = 0; i < schedule.length; i++) {
            int action = (int)(Math.random() * METHODS) + 1;
            switch (action) {
                case 1: {
                    if(!play()) {
                        schedule[i] = i + " - ел";
                        eat(12);
                        break;
                    }
                    schedule[i] = i + " - играл";
                    break;
                }
                case 2: {
                    if(!sleep()) {
                        schedule[i] = i + " - ел";
                        eat(11);
                        break;
                    }
                    schedule[i] = i + " - спал";
                    break;
                }
                case 3: {
                    if(!wash()) {
                        schedule[i] = i + " - ел";
                        eat(15);
                        break;
                    }
                    schedule[i] = i + " - умывался";
                    break;
                }
                case 4: {
                    if(!walk()) {
                        schedule[i] = i + " - ел";
                        eat(18);
                        break;
                    }
                    schedule[i] = i + " - гулял";
                    break;
                }
                case 5: {
                    if(!hunt()) {
                        schedule[i] = i + " - ел";
                        eat(22);
                        break;
                    }
                    schedule[i] = i + " - охотился";
                    break;
                }
            }

        }
        return schedule;
    }


    public void setName(String name) {
        this.name = name;
    }
    public String getVoice() {
        return voice;
    }
    public void setVoice(String voice) {
        this.voice = voice;
    }
    public int getSatiety() {
        return satiety;
    }
    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }
    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }

    public static int getCount() {
        return count;
    }
    public Size getSize() {
        return Size.SMALL;
    }
}
