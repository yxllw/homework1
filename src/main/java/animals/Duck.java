package animals;

import model.Size;

public class Duck extends Herbivore implements Fly, Swim, Run, Voice {
    public Duck(String name) {
        super(name);
    }

    public void fly() {
        System.out.println("Duck is flying");
    }
    public void swim() {
        System.out.println("Duck is swimming");
    }
    public void run() {
        System.out.println("Duck is running");
    }
    public String getVoice() {
        return "quack quack";
    }
    public int getSatiety() {
        return satiety;
    }
    public Size getSize() {
        return Size.SMALL;
    }
}
