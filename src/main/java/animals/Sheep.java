package animals;

import model.Size;

public class Sheep extends Herbivore implements Voice {
    public Sheep(String name) {
        super(name);
    }

    public String getVoice() {
        return "bee";
    }
    public int getSatiety() {
        return satiety;
    }
    public Size getSize() {
        return Size.MEDIUM;
    }
}
