package animals;

import model.Size;

public class Fish extends Carnivorous implements Swim {
    public Fish(String name) {
        super(name);
    }

    public void swim() {
        System.out.println("Fish is swimming");
    }
    public int getSatiety() {
        return satiety;
    }
    public Size getSize() {
        return Size.MEDIUM;
    }
}
