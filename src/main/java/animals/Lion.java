package animals;

import model.Size;

public class Lion extends Carnivorous implements Run {
    public Lion(String name) {
        super(name);
    }
    public void run() {
        System.out.println("Lion is running");
    }
    public int getSatiety() {
        return satiety;
    }
    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }
    public Size getSize() {
        return Size.LARGE;
    }
}
