package animals;


import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {
    public Carnivorous(String name) {
        super(name);
    }
    public void eat(Food food) throws WrongFoodException {
        if(food instanceof Grass) {
            throw new WrongFoodException("Такая еда не подходит для этого животного.");
        }
        else {
            satiety += 40;
            System.out.println("Животное покормлено.");
        }
    }
}
