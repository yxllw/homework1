package model;

import java.util.HashMap;

public class Aviary <T extends animals.Animal> {
    private Size size;
    private HashMap<String, T> aviaryMap = new HashMap<>();
    public Aviary(Size size) {
        this.size = size;
    }

    public void addAnimal(T animal) {
        if(size != animal.getSize()) {
            throw new WrongSizeException("Размер животного отличается от размера вольера.");
        }
        aviaryMap.put(animal.getName(), animal);
    }
    public T getAnimal(String name) {
        return aviaryMap.get(name);
    }
    public boolean removeAnimal(String name) {
        aviaryMap.remove(name);
        return !(aviaryMap.containsKey(name));
    }
    public void getAns() {
        System.out.println(aviaryMap.values());
        System.out.println(aviaryMap.size());
    }
}
