package model;

public class WrongSizeException extends RuntimeException {
    public WrongSizeException(String warning) {
        super(warning);
    }
}
