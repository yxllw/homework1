import animals.*;
import employee.Worker;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import model.Aviary;
import model.Size;

public class Zoo {
    private static Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.SMALL);
    private static Aviary<Herbivore> herbivoreAviary = new Aviary<>(Size.MEDIUM);

    public static void main(String[] args) throws WrongFoodException {
        Cow cow = new Cow("Cow");
        Duck duck = new Duck("Duck");
        Fish fish = new Fish("Fish");
        Kotik kotik = new Kotik("Kot");
        Lion lion = new Lion("Lion");
        Sheep sheep = new Sheep("Sheep");
        Worker worker = new Worker();
        Grass grass = new Grass();
        Meat meat = new Meat();


        worker.feed(cow, grass);
        worker.getVoice(cow);
        worker.feed(kotik, meat);
        worker.feed(fish, meat);
        worker.getVoice(sheep);
        lion.run();
        worker.feed(duck, grass);
        System.out.println("duck satiety = " + duck.getSatiety());
        System.out.println(cow.getSatiety());

        for (Swim i:createPond()) {
            i.swim();
        }

        System.out.println(duck.getSize());
        System.out.println(sheep.getName());


        fillCarnivorousAviary();
        fillHerbivoreAviary();
        carnivorousAviary.getAns();
        herbivoreAviary.getAns();
        System.out.println(carnivorousAviary.removeAnimal("Kotik"));
        carnivorousAviary.getAns();
        System.out.println(herbivoreAviary.removeAnimal("Sheep1"));
        herbivoreAviary.getAns();
        System.out.println(getCarnivorous("Kotik"));
        System.out.println(getHerbivore("Sheep1"));
    }

    public static Swim[] createPond() {
        int n = 5;
        Swim[] array = new Swim[n];
        Duck duck = new Duck("Duck");
        Duck duck1 = new Duck("Duck2");
        Fish fish = new Fish("Fish");
        Fish fish1 = new Fish("Fish2");
        Fish fish2 = new Fish("Fish3");
        array[0] = duck;
        array[1] = duck1;
        array[2] = fish;
        array[3] = fish1;
        array[4] = fish2;
        return array;
    }
    public static void fillCarnivorousAviary() {
        Kotik kotik = new Kotik("Kotik");
        Kotik kotik1 = new Kotik("Kotik1");
        carnivorousAviary.addAnimal(kotik);
        carnivorousAviary.addAnimal(kotik1);
        System.out.println("Животные добавлены в вольер");
    }
    public static void fillHerbivoreAviary() {
        Sheep sheep = new Sheep("Sheep1");
        Sheep sheep1 = new Sheep("Sheep2");
        herbivoreAviary.addAnimal(sheep);
        herbivoreAviary.addAnimal(sheep1);
        System.out.println("Животные добавлены в вольер");
    }
    public static Carnivorous getCarnivorous(String name) {
        fillCarnivorousAviary();
        return carnivorousAviary.getAnimal(name);
    }
    public static Herbivore getHerbivore(String name) {
        fillHerbivoreAviary();
        return herbivoreAviary.getAnimal(name);
    }
}
