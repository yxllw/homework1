package food;

public class WrongFoodException extends Exception {
    public WrongFoodException(String warning) {
        super(warning);
    }
}
